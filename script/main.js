// слайдер на синем фоне
const sliderListSelector      = document.querySelector('.slider-list-selector-block');    // контейнер переключателей слева
const sliderListContent       = document.querySelector('.slider-list-content');           // контейнер текстовых блоков справа
const sliderListContentBlocks = document.querySelectorAll('.slider-list-content-block');  // текстовые блоки справа
// подсветка текста
const textHighlightingBlocks = document.querySelectorAll('.text-highlighting > span'); // блоки с текстом для подсветки



// функция переключения текстовых блоков при скролле
let oldScroll = 0;
function toggleSliderListSelector() {
  const containerProps = sliderListContent.getBoundingClientRect();
  const list           = sliderListSelector.children;
  const scrollTop      = document.documentElement.scrollTop;

  setTimeout(() => oldScroll = document.documentElement.scrollTop, 200);

  sliderListContentBlocks.forEach(v => {
    const start = +v.dataset.controllPointStart;
    const end   = +v.dataset.controllPointEnd;

    // если перед областью
    if (scrollTop < start && oldScroll > scrollTop) {
      setTimeout(() => v.style.display = 'none', 600);
      v.classList.remove('slider-list-content-block-animation-in');
      v.classList.add('slider-list-content-block-animation-out');
    }
    // если внутри области триггера
    if (start <= scrollTop && scrollTop <= end) {
      for (let j = 0; j < list.length; j++) {
        list[j].classList.remove('active');
        if (+v.dataset.sliderSelectorId == +list[j].dataset.sliderSelectorId) list[j].classList.add('active');
      }
      v.removeAttribute('style');
      v.classList.add('slider-list-content-block-animation-in');
      v.classList.remove('slider-list-content-block-animation-out');
    }
    // если после области
    if (scrollTop > end && oldScroll < scrollTop) {
      setTimeout(() => v.style.display = 'none', 600);
      v.classList.remove('slider-list-content-block-animation-in');
      v.classList.add('slider-list-content-block-animation-out');
    }

    if (v.getBoundingClientRect().top < containerProps.top) v.style.display = 'none';
  });
}


// функция подсветки текста
function textHighlighting() {
  let i = 0;
  for(i; i < textHighlightingBlocks.length; i++) {
    if (textHighlightingBlocks[i].getBoundingClientRect().top < document.documentElement.clientHeight * 0.66) {
      textHighlightingBlocks[i].classList.add('active');
    }
  }
}


// общий колбэк на скролл
function scrollAll() {
  // если экран шире 768рх, то тогда запускаем слайдер переключатель
  if (document.documentElement.clientWidth > 786) {
    if (sliderListSelector) toggleSliderListSelector();
  }
  if (textHighlightingBlocks) textHighlighting();
}

// если экран шире 768рх, то тогда запускаем слайдер переключатель
if (document.documentElement.clientWidth > 786) {
  const setControllPoints = () => {
    const containerProps = sliderListContent.getBoundingClientRect();
    const spaceHBlock    = containerProps.height / sliderListContentBlocks.length - 200;

    sliderListContentBlocks.forEach((v,i) => {
      v.dataset.controllPointStart = `${spaceHBlock * i + containerProps.top + document.documentElement.scrollTop - 100}`;
      v.dataset.controllPointEnd = `${spaceHBlock * i + containerProps.top + document.documentElement.scrollTop + spaceHBlock}`;
    });
  };
  if (sliderListSelector)  {
    // устанавливаем тригеры
    setControllPoints();
    // на всякий случай, чтобы обновлялись тригеры при ресайзе
    window.addEventListener('resize', setControllPoints);
  }
}

window.addEventListener('scroll', scrollAll);